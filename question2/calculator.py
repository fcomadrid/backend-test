import re


def isAddTen(a, b):
    return int(a)+int(b) == 10


def dotSeparator(value):
    return value.split('...')


def check(value):
    print(value)
    pattern = re.compile('[\w]*[...]{3}[\w]*', re.VERBOSE)
    return pattern.match(value)

def resolve(value):
    if (not check(value)):
        return False
    
    arr = dotSeparator(value)
    
    arrNumbers = []
    
    for i in arr:
        numbers = getValues(i)
        if(numbers.__len__() == 0):
            return False
        if (numbers.__len__() == 1):
            arrNumbers.append(numbers[0])
        if (numbers.__len__() == 2):
            arrNumbers.append(numbers[0])
            arrNumbers.append(numbers[1])
        if (numbers.__len__() > 2):
            arrNumbers.append(numbers[0])
            arrNumbers.append(numbers.pop())
    while(arrNumbers.__len__() > 1):
        if (not isAddTen(arrNumbers.pop(0), arrNumbers.pop(0))):
            return False
    return True    
    
def getValues(value):
    pattern = re.compile('[1-9]')
    return pattern.findall(value)
            

resolve('sda7d....d3sda3sd...d7asda')
