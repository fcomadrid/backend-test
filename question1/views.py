from django.shortcuts import render
from django.http import HttpResponse
from question1.calculator import resolve
# Create your views here.

def showNumbers(request):
    text = '<html><body>'
    for i in range(50, 151, 1):
        text += str(resolve(i)) + '\n'
    text += '</body></html>' 
    return HttpResponse(text)
