def isMultipleOf(value, multiple):
    return value % multiple == 0


def isFiveMultiple(value):
    return isMultipleOf(value, 5)


def isSevenMultiple(value):
    return isMultipleOf(value, 7)


def resolve(value):
    fiveMultiple = isFiveMultiple(value)
    sevenMultiple = isSevenMultiple(value)
    if(fiveMultiple and sevenMultiple):
        return "FooBar"
    elif (fiveMultiple):
        return "Foo"
    elif (sevenMultiple):
        return 'Bar'
    else:
        return value
