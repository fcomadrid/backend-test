# backend-test

# To build Project Must Run the following scripts:

docker-compose build
docker-compose run

## The first test is in:

http://localhost:8000/question-1

## And the second is in:

http://localhost:8000/question-2?q={queryString}