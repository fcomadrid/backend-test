FROM python:3
ENV PYTHONUNBUFFERED 1
ADD requirements.txt ./
WORKDIR ./
RUN pip install -r requirements.txt
ADD . /backendtest