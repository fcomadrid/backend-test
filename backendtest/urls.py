from django.contrib import admin
from django.urls import path
from question1.views import showNumbers
from question2.views import isAddTen
urlpatterns = [
    path('question-1', showNumbers),
    path('question-2', isAddTen)
]
